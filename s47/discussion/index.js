// querySelector() is a method that can be used to select a specific from our document.
console.log(document.querySelector("#txt-first-name"));

// document refers to the whole page
console.log(document);

/*
	Alternative ways to access HTML elements. This is what we can use aside from the querySelector().

	document.getElementById("txt-first-name");
	document.getElementsByClassName("txt-first-name");
	document.getElementsByTagName("input");
*/

console.log(document.getElementById("txt-first-name"));

// ====== EVENT and EVENT LISTENERS =========
const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");
const txtLastName = document.querySelector("#txt-last-name");

console.log(txtFirstName);
console.log(spanFullName);

/*

	Event
		ex: clock, hover, keypress and many other events

	Event Listeners
		Allows us to let our user/s intereact with our page. With eaach clck or hover there is an event which triggers a function/task.

	Syntax:
		selectedElement.addEventListener("event", function);

*/

txtFirstName.addEventListener("keyup", (event) => {

    // "innerHTML" property retrieves the HTML content within the element
    //  "value" property retrieves the value from the HTML element
    spanFullName.innerHTML = txtFirstName.value
});

// Alternative way to write the code for event handling
txtLastName.addEventListener("keyup", printLastName);

function printLastName(event) {
    spanFullName.innerHTML = txtLastName.value
}

txtFirstName.addEventListener("keyup", event => {
    console.log(event);
    console.log(event.target);
    console.log(event.target.value);
})


function updateFullName(event) {
    const firstName = txtFirstName.value;
    const lastName = txtLastName.value;
    spanFullName.innerHTML = `${firstName} ${lastName}`;
}

const lblFirstName = document.querySelector("#lbl-first-name");

lblFirstName.addEventListener("click", (event) => {
    alert("hello");
});


function submitFullName() {
    const firstName = document.querySelector("#txt-first-name").value;
    const lastName = document.querySelector("#txt-last-name").value;
    const fullNameSpan = document.querySelector("#span-full-name");

    fullNameSpan.innerHTML = `${firstName} + ${lastName}`;
}
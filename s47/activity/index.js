function updateFullName() {
    const firstName = document.querySelector("#txt-first-name").value;
    const lastName = document.querySelector("#txt-last-name").value;
    const fullNameSpan = document.querySelector("#span-full-name");
    fullNameSpan.innerHTML = `${firstName} ${lastName}`;
}

const txtFirstName = document.querySelector("#txt-first-name");
txtFirstName.addEventListener("keyup", updateFullName);

const txtLastName = document.querySelector("#txt-last-name");
txtLastName.addEventListener("keyup", updateFullName);